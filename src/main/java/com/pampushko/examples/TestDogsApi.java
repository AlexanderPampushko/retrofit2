package com.pampushko.examples;

import com.pampushko.examples.pojo.MainPojo;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by algernon on 29/08/2017.
 */
public interface TestDogsApi
{
	@GET("/api/breeds/list/all")
	Call<MainPojo> getDoge();
	
}
