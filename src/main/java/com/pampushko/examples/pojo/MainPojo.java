package com.pampushko.examples.pojo;

/**
 * конвертер json to pojo
 * http://pojo.sodhanalibrary.com/
 */
public class MainPojo
{
	private Message message;
	
	private String status;
	
	public Message getMessage ()
	{
		return message;
	}
	
	public void setMessage (Message message)
	{
		this.message = message;
	}
	
	public String getStatus ()
	{
		return status;
	}
	
	public void setStatus (String status)
	{
		this.status = status;
	}
	
	@Override
	public String toString()
	{
		return "ClassPojo [message = "+message+", status = "+status+"]";
	}
}
