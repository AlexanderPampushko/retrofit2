package com.pampushko.examples;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.pampushko.examples.pojo.MainPojo;
import com.pampushko.examples.pojo.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * Created by algernon on 18/07/2017.
 */
public class Main
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static TestDogsApi testDogsApi;
	
	private Retrofit retrofit;
	
	public static void main(String[] args) throws IOException
	{
		Main app = new Main();
		app.start();
	}
	
	public void start()
	{
		try
		{
			retrofit = new Retrofit.Builder()
					.baseUrl("https://dog.ceo/api/breeds/list/all/")
					.addConverterFactory(GsonConverterFactory.create(new Gson()))
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.build();
			
			TestDogsApi testDogsApi = retrofit.create(TestDogsApi.class);
			Call<MainPojo> doge = testDogsApi.getDoge();
			Response<MainPojo> response = doge.execute();
			Message message = response.body().getMessage();
			Stream.of(message.getTerrier()).forEach(System.out::println);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	private static void test() throws IOException
	{
		System.out.println(System.getProperty("env.zhopa"));
		Properties properties = new Properties();
		URL resource = Resources.getResource("test.properties");
		properties.load(new FileInputStream(resource.getPath()));
		System.out.println(properties.getProperty("jdbc.username"));
		
		log.info("Hello");
	}
	
}
